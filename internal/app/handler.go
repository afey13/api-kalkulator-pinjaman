package app

import (
	healthcheckweb "gitlab.com/afey13/api-kalkulator-pinjaman/internal/app/healthcheck/delivery/web"
	loanweb "gitlab.com/afey13/api-kalkulator-pinjaman/internal/app/loan/delivery/web"
)

// jhipster-needle-import-handler

type RequiredHandlers struct {
	HealthCheckHTTPHandlerRegistry *healthcheckweb.HealthCheckHandlerRegistry
	ExampleHTTPHandlerRegistry     *loanweb.HandlerRegistry
}
