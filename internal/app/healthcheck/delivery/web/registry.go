package web

import (
	"github.com/go-chi/chi"
	"github.com/hellofresh/health-go/v3"
)

type HealthCheckHandlerRegistry struct {
}

func NewHandlerRegistry() *HealthCheckHandlerRegistry {
	return &HealthCheckHandlerRegistry{}
}

func (h HealthCheckHandlerRegistry) RegisterRoutesTo(r chi.Router) error {
	r.Get("/_health", health.HandlerFunc)
	return nil
}
