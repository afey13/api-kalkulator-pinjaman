package healthcheck

import (
	"gitlab.com/afey13/api-kalkulator-pinjaman/internal/app/healthcheck/delivery/web"

	"github.com/google/wire"
)

var ModuleSet = wire.NewSet(
	web.NewHandlerRegistry,
)
