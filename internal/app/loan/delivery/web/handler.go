package web

import (
	"net/http"
	"strconv"
	"time"

	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/log"
	"gitlab.com/aerilyn/service-library/response"
	"gitlab.com/afey13/api-kalkulator-pinjaman/internal/app/loan/usecase"
)

func CalculatorLoanWebHandler(uc usecase.Calculator) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		plafon, errResult := strconv.ParseFloat(r.URL.Query().Get("plafon"), 32)
		if errResult != nil {
			response.Response(log.ResponseOpts{w, r, ctx, nil, 0, errors.NewInternalSystemError()})
			return
		}
		bunga, errResult := strconv.ParseFloat(r.URL.Query().Get("bunga"), 32)
		if errResult != nil {
			response.Response(log.ResponseOpts{w, r, ctx, nil, 0, errors.NewInternalSystemError()})
			return
		}
		lamaPinjaman, errResult := strconv.Atoi(r.URL.Query().Get("lama_pinjaman"))
		if errResult != nil {
			response.Response(log.ResponseOpts{w, r, ctx, nil, 0, errors.NewInternalSystemError()})
			return
		}
		tanggalMulai := r.URL.Query().Get("tanggal_mulai")
		dateTanggalMulai, errResult := time.Parse("2006-01-02", tanggalMulai)
		if errResult != nil {
			response.Response(log.ResponseOpts{w, r, ctx, nil, 0, errors.NewInternalSystemError()})
			return
		}
		cmd := usecase.CalculatorCommand{
			Plafon:              float32(plafon),
			SukuBunga:           float32(bunga),
			LamaPinjaman:        lamaPinjaman,
			TanggalMulaiAnsuran: dateTanggalMulai,
		}
		result, err := uc.Calculator(ctx, cmd)
		if err != nil {
			response.Response(log.ResponseOpts{w, r, ctx, nil, 0, err})
			return
		}
		response.Response(log.ResponseOpts{w, r, ctx, result, http.StatusOK, nil})
	}
}
