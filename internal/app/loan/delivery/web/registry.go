package web

import (
	"gitlab.com/afey13/api-kalkulator-pinjaman/internal/app/loan/usecase"

	"github.com/go-chi/chi"
)

type RegistryOptions struct {
	Calculator usecase.Calculator
}

type HandlerRegistry struct {
	options RegistryOptions
}

func NewHandlerRegistry(options RegistryOptions) *HandlerRegistry {
	return &HandlerRegistry{
		options: options,
	}
}

func (h *HandlerRegistry) RegisterRoutesTo(r chi.Router) error {
	r.Route("/loan-calculator/", func(r chi.Router) {
		r.Get("/", CalculatorLoanWebHandler(h.options.Calculator))
	})
	return nil
}
