package usecase

import (
	"context"
	"time"

	"gitlab.com/aerilyn/service-library/errors"
)

type CalculatorCommand struct {
	Plafon              float32   `json:"plafon"`
	LamaPinjaman        int       `json:"lama_pinjaman"`
	SukuBunga           float32   `json:"suku_bunga"`
	TanggalMulaiAnsuran time.Time `json:"tanggal_mulai_angsuran"`
}

type CalculatorDTO struct {
	Angsuran []Angsuran `json:"angsuran"`
}

type Angsuran struct {
	Number        int     `json:"anguran_ke"`
	Tanggal       string  `json:"tanggal"`
	TotalAngsuran float32 `json:"total_angsuran"`
	AngsuranPokok float32 `json:"angsuran_pokok"`
	AngsuranBunga float32 `json:"angsuran_bunga"`
	SisaAngsuran  float32 `json:"sisa_angsuran_pokok"`
}

type Calculator interface {
	Calculator(ctx context.Context, cmd CalculatorCommand) (*CalculatorDTO, errors.CodedError)
}
