package usecase

//go:generate mockgen -source=createValidator.go -destination=usecasemock/createValidator_mock.go -package=usecasemock

import (
	"context"

	"gitlab.com/aerilyn/service-library/errors"
)

type CalculatorValidator interface {
	Validate(ctx context.Context, cmd CalculatorCommand) errors.CodedError
}
