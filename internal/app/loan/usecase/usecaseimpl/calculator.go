package usecaseimpl

import (
	"context"

	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/afey13/api-kalkulator-pinjaman/internal/app/loan/usecase"
)

const (
	X       float32 = 1
	Percent float32 = 100
)

type CalculatorOptions struct {
	Config              config.ConfigEnv
	CalculatorValidator usecase.CalculatorValidator
}

type Calculator struct {
	options CalculatorOptions
}

func NewCalculator(options CalculatorOptions) *Calculator {
	return &Calculator{
		options: options,
	}
}

func (s *Calculator) Calculator(ctx context.Context, cmd usecase.CalculatorCommand) (*usecase.CalculatorDTO, errors.CodedError) {
	anuitas, err := s.calculateAnuitas(cmd)
	if err != nil {
		return nil, err
	}
	dto := usecase.CalculatorDTO{}

	angsuran := usecase.Angsuran{
		AngsuranPokok: *anuitas,
	}

	dto.Angsuran = []usecase.Angsuran{
		angsuran,
	}

	return &dto, nil
}

func (s *Calculator) calculateAnuitas(cmd usecase.CalculatorCommand) (*float32, errors.CodedError) {
	anuitas := cmd.Plafon * (X + (cmd.SukuBunga / Percent))
	return &anuitas, nil
}
