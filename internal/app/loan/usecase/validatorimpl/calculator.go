package validatorimpl

import (
	"context"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/aerilyn/service-library/errors"
	"gitlab.com/aerilyn/service-library/validation/ozzomapper"
	"gitlab.com/afey13/api-kalkulator-pinjaman/internal/app/loan/usecase"
)

type CalculatorValidator struct {
}

func NewCalculatorValidator() *CalculatorValidator {
	return &CalculatorValidator{}
}

func (s CalculatorValidator) Validate(ctx context.Context, cmd usecase.CalculatorCommand) errors.CodedError {
	validationErr := validation.ValidateStruct(
		&cmd,
		validation.Field(&cmd.Plafon, validation.Required),
		validation.Field(&cmd.LamaPinjaman, validation.Required),
		validation.Field(&cmd.SukuBunga, validation.Required),
		validation.Field(&cmd.TanggalMulaiAnsuran, validation.Required),
	)
	switch e := validationErr.(type) {
	case validation.InternalError:
		return errors.NewInternalSystemError().CopyWith(errors.Message(e.Error()))
	case validation.Errors:
		invalidParameterErr := errors.NewInvalidParameterError()
		return ozzomapper.WrapValidationError(invalidParameterErr.Code(), invalidParameterErr.MessageKey(), invalidParameterErr.Message(), e)
	}
	return nil
}
