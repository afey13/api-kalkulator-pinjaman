package loan

import (
	"gitlab.com/afey13/api-kalkulator-pinjaman/internal/app/loan/delivery/web"
	"gitlab.com/afey13/api-kalkulator-pinjaman/internal/app/loan/usecase"
	"gitlab.com/afey13/api-kalkulator-pinjaman/internal/app/loan/usecase/usecaseimpl"
	"gitlab.com/afey13/api-kalkulator-pinjaman/internal/app/loan/usecase/validatorimpl"

	"github.com/google/wire"
)

var ModuleSet = wire.NewSet(
	repositoryModuleSet,
	validatorModuleSet,
	usecaseModuleSet,
	deliveryModuleSet,
)

var repositoryModuleSet = wire.NewSet()

var deliveryModuleSet = wire.NewSet(
	wire.Struct(new(web.RegistryOptions), "*"),
	web.NewHandlerRegistry,
)

var validatorModuleSet = wire.NewSet(
	validatorimpl.NewCalculatorValidator,
	wire.Bind(new(usecase.CalculatorValidator), new(*validatorimpl.CalculatorValidator)),
)

var usecaseModuleSet = wire.NewSet(
	wire.Struct(new(usecaseimpl.CalculatorOptions), "*"),
	usecaseimpl.NewCalculator,
	wire.Bind(new(usecase.Calculator), new(*usecaseimpl.Calculator)),
)
