package app

import (
	"gitlab.com/aerilyn/service-library/middleware/log"
)

type RequiredMiddlewares struct {
	Logging *log.LoggingMiddlewareRegistry
	// Header  *header.HeaderMiddlewareRegistry
	// NewRelic      *newrelic.NewReliMiddlewareRegistry
	// Auth          *auth.AuthMiddlewareRegistry
	// Authorization *authorization.AuthorizationMiddlewareRegistry
	// Watermill     *watermill.WatermillStdMiddlewareRegistry
}
