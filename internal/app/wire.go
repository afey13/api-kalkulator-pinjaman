//go:build wireinject
// +build wireinject

package app

import (
	"gitlab.com/aerilyn/service-library/config"
	"gitlab.com/aerilyn/service-library/cryptography"
	"gitlab.com/aerilyn/service-library/cryptography/aes256"
	pkgMD5 "gitlab.com/aerilyn/service-library/hash/md5"
	"gitlab.com/aerilyn/service-library/httpRequest"
	httpRequestUsecase "gitlab.com/aerilyn/service-library/httpRequest/usecase"
	"gitlab.com/aerilyn/service-library/jwt"
	"gitlab.com/aerilyn/service-library/middleware/auth"
	"gitlab.com/aerilyn/service-library/middleware/authorization"
	"gitlab.com/aerilyn/service-library/middleware/header"
	"gitlab.com/aerilyn/service-library/middleware/log"
	"gitlab.com/aerilyn/service-library/middleware/pubsub/watermill"
	"gitlab.com/aerilyn/service-library/module"
	"gitlab.com/aerilyn/service-library/otel/newrelic"
	"gitlab.com/aerilyn/service-library/pubsub"
	"gitlab.com/afey13/api-kalkulator-pinjaman/internal/app/healthcheck"
	loanApp "gitlab.com/afey13/api-kalkulator-pinjaman/internal/app/loan"
	"gitlab.com/afey13/api-kalkulator-pinjaman/internal/pkg/cache"
	"gitlab.com/afey13/api-kalkulator-pinjaman/internal/pkg/cache/redis"
	"gitlab.com/afey13/api-kalkulator-pinjaman/internal/pkg/database/mongo"
	"gitlab.com/afey13/api-kalkulator-pinjaman/internal/pkg/email/gmail"
	"gitlab.com/afey13/api-kalkulator-pinjaman/internal/pkg/pubsub/rabbitmq"
	"gopkg.in/gomail.v2"

	"github.com/google/wire"
)

var ModuleSet = wire.NewSet(
	wire.Struct(new(RequiredHandlers), "*"),
	wire.Struct(new(RequiredMiddlewares), "*"),
	wire.Struct(new(ModuleOptions), "*"),
	wire.Struct(new(aes256.Aes256Opts), "*"),
	wire.Struct(new(auth.AuthMiddlewareOpts), "*"),
	wire.Struct(new(authorization.AuthorizationMiddlewareRegistryOptions), "*"),
	wire.Struct(new(header.HeaderMiddlewareOpts), "*"),
	wire.Bind(new(cache.Cache), new(*redis.RedisClient)),
	wire.Bind(new(gmail.GomailDialer), new(*gomail.Dialer)),
	wire.Bind(new(jwt.Jwt), new(*jwt.JwtOpts)),
	wire.Bind(new(config.ConfigEnv), new(*config.Config)),
	wire.Bind(new(httpRequest.HttpRequestHelper), new(*httpRequestUsecase.HttpRequestHelper)),
	wire.Bind(new(cryptography.Cryptography), new(*aes256.Aes256)),
	wire.Bind(new(pkgMD5.HashMD5), new(*pkgMD5.HashMD5Impl)),
	NewModule,
	config.InjectConfig,
	config.ProvideConfig,
	mongo.ProvideClient,
	redis.ProvideClient,
	newrelic.ProvideNewRelic,
	log.NewLoggingMiddlewareRegistry,
	newrelic.NewNewReLicMiddlewareRegistry,
	auth.NewAuthMiddlewareRegistry,
	authorization.NewAuthorizationMiddlewareRegistry,
	header.NewHeaderMiddlewareRegistry,
	httpRequestUsecase.NewHttpRequestHelper,
	jwt.ProvideJwt,
	aes256.ProvideAes256,
	pkgMD5.NewHashMD5,
	module.NewApplicationDelegate,
	pubsub.NewRunner,
	pubsub.ProvideLoggerWithStdLogger,
	pubsub.ProvideDurableQueueConfig,
	pubsub.ProvideRouter,
	rabbitmq.NewProviderSubscriberWithAMQP,
	rabbitmq.NewProviderPublihserWithAMQP,
	gmail.ProviderGmail,
	watermill.NewWatermillStdMiddlewareRegistry,
)

func InjectApp() (*Module, func(), error) {
	panic(wire.Build(
		ModuleSet,
		healthcheck.ModuleSet,
		loanApp.ModuleSet,
	))
}
